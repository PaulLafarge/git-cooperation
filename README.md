# git-cooperation

A partir d'un repo git existant (https://gitlab.com/simplon-fad-iot-labege-1/week-7/git-cooperation ), 
en ligne de commande exclusivement vous:
===========================================================================================
$ git clone https://gitlab.com/simplon-fad-iot-labege-1/week-7/git-cooperation
=> le repository git-cooperation est cloné en local dans le répertoire courant
$ cd git-cooperation
=> On se positionne dans le répertoire git-cooperation nouvellement créé par la commande clone
$ git status
=> Un message indique que l'on se trouve sur la branche master
$ ls
=> La branche MASTER est bien initialisée avec le README.md

- ajoutez une branche dev
===========================
$ git checkout -b dev
==> La branche dev est créée et on bascule sur dev
$ git status
==> Le status montre que l'on se trouve sur la branche dev

- ajoutez un fichier main.c sur cette branche
===============================================
$ echo "/* Main file */" > main.c
$ echo "#include <stdio.h>" >> main.c
$ echo "int main(void) {" >> main.c
$ echo "printf(\"Hello world\n\");" >> main.c
$ echo "}" >> main.c
==> Creation du fichier main.c

$ git status
==> le fichier main.c apparait en rouge, dans la rubrique <fichiers non suivis> 
$ git add main.c
==> le fichier est ajouté dans l'espace local de git
$ git status
==> le fichier main.c apparait en vert, dans la rubrique modifications qui seront validées

- mettez à jour le repo distant avec cette ajout de code
=========================================================
$ git commit -m "My first hello world file"
==> le fichier main.c est dans l'ensemble des fichiers prêts à être envoyés dans le repo distant"
$ git status
==> Indique: sur la branche dev, rien à valider
$ git push -u origin dev
==> le fichier main.c est dans le repository distant

- fusionnez la branch dev dans master
=======================================
$ git checkout master
==> On se positionne sur le master
$ git merge dev
==> On prepare le merge du dev
$ git push -u "origin/master" 
==> On envoie le merge dans le master

- supprimez la branche dev
==========================
$ git branch -d dev
==> La branche locale est supprimée

- l'état de votre repo local doit être le même que le repo remote
=================================================================
$ git push 
==> La branche est supprimée dans le repo remote

